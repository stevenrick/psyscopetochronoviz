#psyScopeToChronoViz

This python script converts PsyScope subject elicitation logs (in .txt format) into a format compatible for importing into [ChronoViz](http://chronoviz.com/).


##Input Structure Requirements

While the script is forgiving in regards to leading file information and headers, it does assume the actual elicitation input data follows a specific structure:
 ![import](https://bytebucket.org/srick/psyscopetochronoviz/raw/0a3497e24ad6883fb58156bc31c2d89cd54f7617/input.png)


##Using psyScopeToChronoViz

The only requirement is to have Python 2.7 installed.

To run the application enter: python psyScopeToChronoViz.py

Once open, a file dialogue will open asking you to select the file you wish to convert.


##Output

The output file will match the name of the input file but be in .csv format
 ![import](https://bytebucket.org/srick/psyscopetochronoviz/raw/66ac3139b45268c7125486fc549b3c755c321185/output.png)


##Importing into ChronoViz

Once the output is generated, you can import it into ChronoViz by using the Add Data option. Select the file and in the pop up window make certain that the Time Coding is relative, Time Column is StartTime,
 and the lower data columns match the following:
 ![import](https://bytebucket.org/srick/psyscopetochronoviz/raw/70c58fea7e9ce278d5a7557de62d7164db84fb0b/import.png)
 
 