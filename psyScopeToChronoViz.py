'''
Created on May 2, 2014

@author: rick
'''

import re
import csv
import os
import Tkinter, tkFileDialog


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def clean():
    print 'Starting clean'
    
    # define options for opening or saving a file
    
    filename = tkFileDialog.askopenfilename()
    savename = os.path.splitext(filename)[0]+"_temp.csv"
    
    txt_file = r""+filename+""
    csv_file = r""+savename+""
    
    with open(txt_file, 'rb') as inFile, open(csv_file,'wb') as temp_csvOutput:
        
        for line in inFile:
            #print line
            clean_pattern = re.compile(r'[\t\s a-zA-Z0-9\*\(\)_:.,]+') #get rid of garbage unicode
            fixed = clean_pattern.findall(line)
            corrected = ''.join(fixed)
            columns = corrected.split('\t') #split morae columns by tabs
            #print columns
            temp_csvOutput.write( ','.join(columns) )
    
    inFile.close()
    temp_csvOutput.close()
    print 'Finished clean'
    return savename


def convert(tempfile):
    savename = (os.path.splitext(tempfile)[0])[:-5]+".csv"
    temp_file = r""+tempfile+""
    annotation_file = r""+savename+""
    with open(temp_file, "rU") as in_file, open(annotation_file, 'wb') as out_file:
        
        outWriter = csv.writer(out_file, delimiter=',')
        
        #write the file header
        header = 'StartTime,EndTime,Title,Annotation,MainCategory,Category2,Category3'
        writeholder = header+'\n' 
        outWriter.writerow(writeholder.split(','))
        
        startFlag = False
        category = 'Category 1'
        print 'Starting conversion'
        for line in in_file:
            row = line.split(',')
            if row[0] == 'Trial':
                startFlag = True
                category = (row[3])
                #print 'Trail start'
                continue
            if is_number(row[0]) and startFlag:
                #print row[0]
                startTime = (float(row[2])/1000)
                endTime = ((float(row[5])/1000)+startTime)
                annotation = (row[3])
                writeHolder = str(startTime)+','+str(endTime)+',,'+annotation+','+category
                outWriter.writerow(writeHolder.split(','))
            if not is_number(row[0]) and startFlag:
                startFlag = False
                #print 'False start'
            
    print 'Conversion complete'
    out_file.close()
    in_file.close()
    os.remove(tempfile)


root = Tkinter.Tk()
root.withdraw()

tempfile = clean() #function to convert raw psyscope output from tab separated .txt to .csv
convert(tempfile) #function to convert .csv into ChronoViz annotation format